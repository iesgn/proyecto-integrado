# proyecto-integrado

Repositorio del Proyecto de Fin de Ciclo.
IES Gonzalo Nazareno, 2019.

Versiones de las herramientas utilizadas:
- Azure CLI: 2.0.64
- Terraform: 0.12
- Ansible: 2.8.0
- InSpec: 4.3.2
