resource "azurerm_resource_group" "myrsg" {
  name     = "myrsg"
  location = "westeurope"
}

resource "azurerm_virtual_network" "myvnet" {
  name                = "default-vnet"
  address_space       = ["10.0.0.0/16"]
  resource_group_name = azurerm_resource_group.myrsg.name
  location            = azurerm_resource_group.myrsg.location
}

resource "azurerm_subnet" "mysubnet" {
  name                 = "default-subnet"
  address_prefix       = "10.0.10.0/24"
  virtual_network_name = azurerm_virtual_network.myvnet.name
  resource_group_name  = azurerm_resource_group.myrsg.name
}

resource "azurerm_public_ip" "mypip" {
  name                = "default-public-ip"
  allocation_method   = "Dynamic"
  resource_group_name = azurerm_resource_group.myrsg.name
  location            = azurerm_resource_group.myrsg.location
}

resource "azurerm_network_interface" "mynic" {
  name                = "default-nic"
  resource_group_name = azurerm_resource_group.myrsg.name
  location            = azurerm_resource_group.myrsg.location

  ip_configuration {
    name                          = "default-ipconf"
    subnet_id                     = azurerm_subnet.mysubnet.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = azurerm_public_ip.mypip.id
  }
}

resource "azurerm_virtual_machine" "myvm" {
  name                  = "arcabele"
  resource_group_name   = azurerm_resource_group.myrsg.name
  location              = azurerm_resource_group.myrsg.location
  network_interface_ids = [azurerm_network_interface.mynic.id]
  vm_size               = "Standard_D2s_v3"

  delete_os_disk_on_termination    = true
  delete_data_disks_on_termination = true

  storage_image_reference {
    publisher = "credativ"
    offer     = "Debian"
    sku       = "9"
    version   = "latest"
  }

  storage_os_disk {
    name              = "default-os-disk"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
  }

  os_profile {
    computer_name  = "arcabele"
    admin_username = "alvaro"
    admin_password = "P4$$w.rd"
  }

  os_profile_linux_config {
    disable_password_authentication = true
    ssh_keys {
      path     = "/home/alvaro/.ssh/authorized_keys"
      key_data = "${file("~/.ssh/id_rsa.pub")}"
    }
  }
}

