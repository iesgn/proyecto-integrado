control 'azurerm_virtual_machine' do
	title 'Tests sobre Azure' 
	desc 'La máquina virtual nodo1 debe existir'
	describe azurerm_virtual_machine(resource_group: 'myrsg', name: 'nodo1') do
		it { should exist }
		its('name') { should eq('nodo1') }	
		its('location') { should eq('westeurope') }
		its('type') { should eq('Microsoft.Compute/virtualMachines') }
	end
	desc 'La máquina virtual nodo2 debe existir'
	describe azurerm_virtual_machine(resource_group: 'myrsg', name: 'nodo2') do
		it { should exist }
		its('name') { should eq('nodo2') }	
		its('location') { should eq('westeurope') }
		its('type') { should eq('Microsoft.Compute/virtualMachines') }
	end
end

