control 'Postgres' do
	title 'Tests de PostgreSQL'
	desc 'PostgreSQL debe estar instalado'
	describe package('postgresql-9.6') do
		it { should be_installed }
		its('version') { should cmp >= '9.6' }
	end
	desc 'PostgreSQL debe estar activado y funcionando'
	describe service('postgresql') do
		it { should be_enabled }
	end
end
