control 'Nodo 2' do
	title 'Tests del nodo2'
	desc 'El puerto 80 debe ser alcanzable'
	describe host(ENV['IP_NODO2'], port: 80, protocol: 'tcp') do
		it { should be_reachable }
	end
	desc 'telnet no debe estar instalado'
	describe package('telnet') do
		it { should_not be_installed }
	end
end
