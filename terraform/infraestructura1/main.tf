module "myvm" {
  source               = "../modulos/vm1"
  address_space        = "10.0.0.0/16"
  address_prefix       = "10.0.10.0/24"
  virtual_machine_name = "arcabele"
  private_ip_address   = "10.0.10.5"
}
